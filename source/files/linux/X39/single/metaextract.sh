#!/bin/bash
#https://exiftool.org/exiftool_pod.html
trap ctrl_c INT
function ctrl_c() {
    echo "[alert]interrupt trapped"
	echo "[alert]better luch next time!"
	exit
}
outloc=(/metaextract)
exex=(_meta_extract)
function alert {
    clear
    echo "[evidence]script completed successfully"
}
if [ -d $outloc ]
    then
    cd $outloc
    clear
    echo "[!]you are located here $(pwd)"
    read -n 1 -p "[!]are you in the right working directory? care!; If not ctrl+c now!"
    sleep 4
    echo "[!]starting now..."
    #creating random string
    var_rando=$(openssl rand -hex 12)
    find . -type d -empty -delete 2>/dev/null
    fdupes -q -r -q -d -N . 2>/dev/null
    detox -r . 2>/dev/null
    mkdir $exex 2>/dev/null
    exiftool -fast 10 -j *.* -r . > "$outloc"/"$exex"/"$var_rando"_metadata.json
    alert
    echo '[!]metadata json: "$outloc"/"$exex"/"$var_rando"_metadata.json'
    else
    echo "[!]output location was not found, exiting..."
    exit 1
fi