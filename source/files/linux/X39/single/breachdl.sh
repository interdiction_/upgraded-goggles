#!/bin/bash
trap ctrl_c INT
function ctrl_c() {
        echo "[alert]interrupt trapped"
		echo "[alert]better luch next time!"
		exit 1
}
function alert {
    clear
    echo "[evidence] Script completed successfully"
}
function waitfortor {
    sleep 30 
}
function cantconnet {
    echo "[error]cannnot connect..."
    exit 1
}
function canconnect {
    echo "[alert]internet connection stable continuing..."
}
if [ $# -lt 1 ]
    then
        echo "Use --help to see options"
        exit 1
fi
if [ $1 == --help ]
    then
    echo "[banner] Ignore certificate errors, its an known issues with wget2"
    echo "[banner] Download of files @40 threads"
    echo "[usage] docker run -v /workingdir:/breachdl -t --rm -i breachdl:latest http[s]://url X"
    echo "[usage] X: 1 = Single file via tor"
    echo "[usage] X: 2 = Single file via clearnet"
    echo "[usage] X: 3 = Directory via tor"
    echo "[usage] X: 4 = Directory via clearnet"
    exit 1
fi
	if [ $# -ne 2 ]
	then
    echo "[!]syntax error!"
    echo "[!]use --help to see options"
	exit 1
	fi
if [ $2 == 1 ]
	then
    echo "[!]single file via tor..."
    sleep 4
    tor 1>/dev/null &
    echo "[!]launching tor..."
    waitfortor
    if [[ $(torsocks wget -q --tries=10 --timeout=20 --spider https://www.google.com/) -eq 0 ]]
        then
        canconnect
        else
        cantconnet
    fi
    torsocks axel "$1"
    alert
	exit 1
fi
if [ $2 == 2 ]
	then
    echo "[alert]clearnet download enabled"
    if [[ $(wget -q --tries=10 --timeout=20 --spider https://www.google.com/) -eq 0 ]]
        then
        canconnect
        else
        cantconnet
    fi
    axel "$1"
    alert
    exit 1
fi
if [ $2 == 3 ]
	then
    echo "[!]directory via tor..."
    sleep 4
    tor 1>/dev/null &
    echo "[!]launching tor..."
    waitfortor
    if [[ $(torsocks wget -q --tries=10 --timeout=20 --spider https://www.google.com/) -eq 0 ]]
        then
        canconnect
        else
        cantconnet
    fi
    torsocks wget2 -t 10 -r --check-certificate=off --check-hostname=off --robots=off --max-threads 40 --user-agent="Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:21.0) Gecko/20100101 Firefox/21.0" "$1"
    alert
    exit 1
fi
if [ $2 == 4 ]
	then
    echo "[!]clearnet download enabled"
    if [[ $(wget -q --tries=10 --timeout=20 --spider https://www.google.com/) -eq 0 ]]
        then
        canconnect
        else
        cantconnet
    fi
    wget2 -t 10 -r --check-certificate=off --check-hostname=off --robots=off --max-threads 40 --user-agent="Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:21.0) Gecko/20100101 Firefox/21.0" "$1"
    alert
    exit 1
fi