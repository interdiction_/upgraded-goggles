#!/bin/bash
#b591cf16-36c0-4508-8f60-597fe2b140af-Nebul
#docker run -v ${PWD}:/gitscanner -t --rm -i gitscanner:latest https://github.com/url
export var_s5='sleep 5'
export var_netconn='https://www.google.com/'
export var_ss1='sleep 0.1'
export var_s20='sleep 20'
export var_curl30='curl -s --connect-timeout 30'
export var_capi='curl -s -m 10 --connect-timeout 10'
var_out=(/gitscanner/evidence)
LEAKI=(/custom.toml)
trap ctrl_c INT
function ctrl_c() {
        echo "[!]interrupt trapped"
		echo "[!]better luch next time!"
		exit 1
}
#functions
function alert {
    clear
    echo "[!]script completed successfully"
}
function error {
    clear
    echo "[!]script did not complete"
    exit 1
}
func_inet_f () {
	n=0
	until [ "$n" -ge 15 ]
		do
		$var_curl30 $var_netconn >/dev/null && break
		n=$((n+1))
		sleep 5
		done
	if [ "$n" == 0 ]
		then
		$var_ss1
		elif [ "$n" -gt 0 ]
		then
		echo "[f]internet conn failed!"
		exit 1
	fi
}
#
if [ $# -lt 1 ]
    then
        error
        echo "[!]Use --help to see options"
        exit 1
fi
if [ $1 == --help ]
    then
    echo "                      ensure url is passed as the first argument"
    echo "[use] docker run -v ${PWD}:/gitscanner -t --rm -i gitscanner:latest https://github.com/url"
    echo "                                   ---OR AS A DAEMON---"
    echo "[use] docker run -v ${PWD}:/gitscanner --rm -d gitscanner:latest https://github.com/url"
    exit
fi
#check github string in passed argument
if [[ $1 == *"https://github.com/"* ]]
    then
    echo "[!]supplied argument is correct"
    $var_sleep5
    clear
    else
    echo "[!]supplied string doesnt contain correct github"
    echo "[!]please run --help"
    exit 1
fi
#internet connection test
func_inet_f
#save location
if [ -d "$var_out" ] 
    then
    echo "[!]$var_out exists"
    else
    echo "[!]$var_out does not exist, creating directory"
    mkdir "$var_out"
fi
#call and extract all repos for user
var_repo=$(echo "$1" | cut -f4 -d "/")
curl -s -m 10 --connect-timeout 50 -H "Accept: application/vnd.github.v3+json" https://api.github.com/users/"$var_repo"/repos | jq ".[] .full_name" | tr -d '"' > tmp.repo
while read line
    do
    echo "https://github.com/$line" >> main.repo
    done <tmp.repo
rm tmp.repo 2>/dev/null
#creating kill routine for repos
if [[ -f "main.repo" ]]
    then
    echo "[!]repos successfully extracted"
    else
    error
    exit
fi
#extraction loop over main.repo files
while read line
    do
    sleep 20
    git clone "$line"
    var_cloned=$(echo "$line" |rev |cut -f1 -d "/" |rev)
    var_api_call=$(echo "$line" |cut -f4 -d "/")
        if [ -d "$var_cloned" ]
            then
            var_rando=$(openssl rand -hex 12)
            cd "$var_cloned"
            gitleaks --threads=10 --config-path="$LEAKI" --no-git --path=. -o repo_"$var_cloned"_toml_hits_"$var_rando".json
            find . -name '*.zip' -execdir unzip -P x -o '{}' ';'
            /yara_scanner/yara_scanner.py -r -j -Y /yara . >> repo_"$var_cloned"_yara_hits_"$var_rando".json
            echo "$line" > repo_"$var_cloned"_uniq_"$var_rando"_url.log
            $var_capi https://api.github.com/users/"$var_api_call"/events/public > repo_"$var_cloned"_gh_api_"$var_rando".json
            $var_s20
            $var_capi https://api.github.com/repos/"$var_api_call"/"$var_cloned"/commits > repo_"$var_cloned"_gh_api_commits_"$var_rando".json
            cd ..
            mv "$var_cloned" "$var_out"/repo_"$var_cloned"_uniq_"$var_rando"
            find "$var_out"/ -type f -size -6c -exec rm {} \;
            clear
            else
            echo "[!]"$var_cloned" does not exist"
            error
        fi
done <main.repo
rm main.repo 2>/dev/null
alert