#gems required.
require 'net/http'
require 'uri'
def connection_test
    command = Thread.new do
    output = system('wget -q --tries=10 --timeout=20 --spider https://www.google.com/')
    if output
        puts "[alert]internet connection succeed!"
    else
        puts "[error]internet connection failed, quiting..."
        exit
    end
    end
    command.join
    end

def help_menu
    puts `clear`
    puts '[banner]  upgraded goggles'
    puts "[use]     ruby readme.rb --[option]"
    puts "[arg]     lists what the function does (and use), so you can understand it"
    puts '[option]  --gitscanner'
    puts '          --breachdl'
    puts '          --meta'
    puts '          --update'
    puts '          --help'
    puts ""
    puts '[example] ruby readme.rb --meta'
    puts ""
    end
if ARGV.empty?
    help_menu
    exit
    end
if ARGV[0] == "--help" #help menu
    help_menu
elsif ARGV[0] == "--gitscanner" #gitscanner understanding
    command = Thread.new do
    puts `clear`
    puts(<<-EOT)
 Github collection and processing tool based on predefined rule sets that collects/parses Github repositories.
 Gitscanner has [4] main functions:
 [1] Downloads supplied github url
 [2] Extracts and downloads all other repositories of the same user
 [3] Scans repositories for unique Yara and Regex's based on predefined rule sets; and
 [4] Exports all data into JSON for ingestion into splunk or ELK (also stores the output to a predefined location, if triage is nessesary).
 User configured Gitscanner options:
 [1] Custom Yara and Regex rules, based on Yara and TOML rule sets; and
 [2] If the Dockerfile is edited, can pull said Yara and TOML files locally or remotely.
 Requirements:
 [1] Docker
 [2] A code editor of choice; and
 [3] An active internet connection.
 
 [use]: docker run -v ${PWD}:/gitscanner -t --rm -i gitscanner:latest https://github.com/USER/REPO
 
 [exceptions]: Tests internet connection via clearnet, and if it fails, quits.
 [reference]:  https://github.com/x543g/upgraded-goggles/#gitscanner
    EOT
    end
    command.join
    exit
elsif ARGV[0] == "--breachdl" #breachdl understanding
    puts `clear`
    command = Thread.new do
    puts(<<-EOT)
 A multithreaded metadata collection tool.
 Breachdl has [2] main functions:
 [1] Downloads files via clearnet at 40 threads; and
 [2] Downloads files via TOR at 40 threads.
 User selected breachdl options:
 [1] Single file via clearnet
 [2] Single file via TOR
 [3] Directory via clearnet; and
 [4] Directory via TOR.
 
 [use]: docker run -v ${PWD}:/breachdl -t --rm -i breachdl:latest https://URL

 [exceptions]: Tests internet connection via TOR or clearnet, and if it fails, quits.
 [reference] : https://github.com/x543g/upgraded-goggles/#breachdl
    EOT
    end
    command.join
    exit
elsif ARGV[0] == "--meta" #meta understanding
    puts `clear`
    command = Thread.new do
    puts(<<-EOT)
 A simple process to extract metadata and visualise in splunk/ELK.
 Metaextract has [1] main function:
 [1] Parses passed directory and extracts all metadata to JSON, so it can be ingested into splunk or ELK.
 
 [use]: Run from the DIR to be parsed.
 [use]: docker run -v ${PWD}:/metaextract -t --rm -i metaextract:latest
 
 [reference]: https://github.com/x543g/upgraded-goggles/#metaextract
    EOT
    end
    command.join
    exit
elsif ARGV[0] == "--update" #updates the repo to the current version if no changes have been made
    command = Thread.new do
        puts `clear`
        puts '[alert]updating repo'
        sleep 4
        system('bash update.sh -u')
        end
        command.join
        exit
else #small help if no options selected
    help_menu
end