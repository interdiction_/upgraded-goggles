#!/bin/bash
function update_repo {
    git config pull.rebase false
    git pull
}
function error {
	echo "[!]use bash update_repo.sh --help to see options!"
	echo "[!]use bash update_repo.sh -u (to update now!)"
}
if [ $# -lt 1 ]
    then
	clear
	error
	exit
fi
if [ $1 == --help ]
    then
	clear
	echo "[!]bash update_repo.sh -u"
	exit
fi
if [ $1 == -u ]
	then
	clear
	update_repo
	exit
	else
	clear
	echo "[!]incorrect arguments passed"
	error
	exit
fi