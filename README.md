![codebase](https://img.shields.io/badge/codebase-ruby-red)![codebase](https://img.shields.io/badge/codebase-bash-black)![codebase](https://img.shields.io/badge/codebase-go-green)![codebase](https://img.shields.io/badge/codebase-python-blue)

>A thinker sees his own actions as experiments and questions–as attempts to find out something. Success and failure are for him answers above all.- Friedrich Nietzsche

<div align="center">

<img src="source/files/images/visor.gif" width="300">

**All tools output results in JSON**
</div>

### Usage

```
git clone https://gitlab.com/interdiction_/upgraded-goggles
```
What does what?

```
cd upgraded-goggles
ruby readme.rb --help
[banner]  upgraded goggles
[use]     ruby readme.rb --[option]
[arg]     lists what the function does (and use), so you can understand it
[option]  --gitscanner
          --breachdl
          --meta
          --attkfactory [WIP|NOT IMP]
          --update
          --help
[example] ruby readme.rb --meta
```

###### Gitscanner

>Github collection and processing tool based on predefined rule sets that collects/parses Github repositories.

```
cd upgraded-goggles/docker/single
#put your own custom yara dir in this dir!
#put your own custom toml file in this dir!
docker build -t gitscanner . --no-cache
docker run -v ${PWD}:/gitscanner -t --rm -i gitscanner https://github.com/user/repo
docker run -v ${PWD}:/gitscanner --rm -d gitscanner https://github.com/user/repo
```
###### Breachdl

>A multithreaded clearnet/tor downloading tool.

```
cd upgraded-goggles/docker/breachdl
docker build -t breachdl . --no-cache
#CD to download dir
docker run -v ${PWD}:/breachdl -t --rm -i breachdl --help
docker run -v ${PWD}:/breachdl -t --rm -i breachdl http[s]://url X
[usage] X: 1 = Single file via tor
[usage] X: 2 = Single file via clearnet
[usage] X: 3 = Directory via tor
[usage] X: 4 = Directory via clearnet
```

##### metaextract

>A simple process to extract metadata and visualise and correlate in splunk. Outputs results in JSON format.

```
cd upgraded-goggles/docker/metaextract
docker build -t metaextract . --no-cache
#CD to you scan directory!!!
docker run -v ${PWD}:/metaextract -t --rm -i metaextract
```

###### Attack Factory [NOT PUBLISHED || PLACEHOLDER]
<img src="source/files/images/sxVZUyAjmS28c09397-10df-4d8b-bb84-fde1c2fb3421-1628154008.png" width="125">

>TBA. Full end to end OSINT automation tool!